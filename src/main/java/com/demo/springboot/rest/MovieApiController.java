package com.demo.springboot.rest;

import com.demo.springboot.dto.MovieDto;
import com.demo.springboot.model.MovieBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class MovieApiController {
    private static final Logger LOG = LoggerFactory.getLogger(MovieApiController.class);
    private final MovieBase movieBase = new MovieBase();

    @PostMapping("/users")
    public ResponseEntity<Void> createMovie(@RequestBody MovieDto movieDto) throws URISyntaxException {
        try {
            if (movieDto.getTitle() == null || movieDto.getYear() == null || movieDto.getImage() == null) {
                throw new Exception();
            }
            LOG.info("--- title: {}", movieDto.getTitle());
            LOG.info("--- year: {}", movieDto.getYear());
            LOG.info("--- image: {}", movieDto.getImage());
            movieBase.addMovie(movieDto);
        } catch (Exception e) {
            LOG.info("--- Wrong params");
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.created(new URI("/api/users/")).build();
    }

    @GetMapping("/users")
    public ResponseEntity<List<MovieDto>> getMovies() {
        LOG.info("--- get all movies: {}", movieBase.getSize());
        return ResponseEntity.ok().body(movieBase.getMovieList());
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<Void> updateMovie(@PathVariable Integer id, @RequestBody MovieDto movieDto) {
        LOG.info("--- id: {}", id);
        try {
            LOG.info("--- modified: {}", movieBase.getTitle(id));
            movieBase.editMovie(id, movieDto);
        } catch (Exception e) {
            LOG.info("--- No movie with such id: {}", id);
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<Void> getMovie(@PathVariable("id") Integer id) {
        LOG.info("--- id: {}", id);
        try {
            LOG.info("--- removed: {}", movieBase.getTitle(id));
            movieBase.removeMovie(id);
        } catch (Exception e) {
            LOG.info("--- No movie with such id: {}", id);
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build();
    }
}
