package com.demo.springboot.model;

import com.demo.springboot.dto.MovieDto;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MovieBase {
    private List<MovieDto> movieList =new ArrayList<>();

    public MovieBase(){
    }

    public MovieBase(List<MovieDto> movieList){
        this.movieList = movieList;
    }

    public void addMovie(MovieDto md){
        movieList.add(md);
    }

    public void editMovie(int id, MovieDto md){
        movieList.set(id, md);
    }

    public List<MovieDto> getMovieList(){
        ArrayList<MovieDto> reversedList = new ArrayList<MovieDto>(movieList);
        Collections.reverse(reversedList);
        return reversedList;
    }

    public int getSize(){
        return movieList.size();
    }

    public void removeMovie(int id){
        movieList.remove(id);
    }

    public String getTitle(int id){
        return movieList.get(id).getTitle();
    }

    public Integer getYear(int id){
        return movieList.get(id).getYear();
    }

    public String getImage(int id){
        return movieList.get(id).getImage();
    }
}
